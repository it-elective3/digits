<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StudentsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'year_id' =>  'required|exists:year_levels,year_id',
            'gender_id' =>  'required|exists:genders,gender_id',
            'section_id' =>  'required|exists:sections,section_id',
            'first_name' =>  'required',
            'middle_name' =>  'required',
            'last_name' =>  'required',
            'age' =>  'required|integer',
            'email' =>  'required|email',
            'contact_no' =>  'required',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
