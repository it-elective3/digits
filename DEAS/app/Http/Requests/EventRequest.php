<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class EventRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'semester_id' =>  'required|exists:semesters,semester_id',
            'school_year_id' =>  'required|exists:school_years,school_year_id',
            'student_monitor_id' =>  'required|exists:students,student_id',
            'event_name' =>  'required',
            'event_date' =>  'required',
            'event_time_in' =>  'required',
            'event_time_in_duration' =>  'required|integer',
            'event_time_out' =>  'required',
            'event_time_out_duration' =>  'required|integer',
            'event_location' =>  'required'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
