<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AttendanceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'student_id' =>  'required|exists:students,student_id',
            'finished_event_id' =>  'required|exists:finished_events,finished_event_id',
            'time_in_status' =>  'required|exists:attendance_statuses,status_id',
            'time_out_status' =>  'required|exists:attendance_statuses,status_id',
            'attendance_payables' =>  'required|integer',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
