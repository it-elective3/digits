<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SemesterRequest;
use App\Http\Controllers\Controller;
use App\Models\semester;

class SemesterController extends Controller
{
    public function index()
    {
        $semester = semester::all();
        return response()->json($semester, 200);
    }
   
    public function store(SemesterRequest $request)
    {
        $semester = semester::create($request->all());
        return response()->json($semester, 201);
    }
}
