<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\PositionRequest;
use App\Http\Controllers\Controller;
use App\Models\position;

class PositionController extends Controller
{
    public function index()
    {
        $position = position::all();

        return response()->json($position, 200);
    }

    public function store(PositionRequest $request)
    {
        $position = position::create($request->all());
        return response()->json($position, 201);
    }
}
