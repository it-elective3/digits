<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\GenderRequest;
use App\Http\Controllers\Controller;
use App\Models\gender;

class GenderController extends Controller
{
    public function index()
    {
        $gender = gender::all();

        return response()->json($gender, 200);
    }

    public function store(GenderRequest $request)
    {
        $gender = gender::create($request->all());
        return response()->json($gender, 201);
    }
}
