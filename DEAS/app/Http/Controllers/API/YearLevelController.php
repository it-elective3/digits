<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\YearLevelRequest;
use App\Http\Controllers\Controller;
use App\Models\year_level;

class YearLevelController extends Controller
{
    public function index()
    {
        $year_level = year_level::all();

        return response()->json($year_level, 200);
    }

    public function store(YearLevelRequest $request)
    {
        $year_level = year_level::create($request->all());
        return response()->json($year_level, 201);
    }
}
