<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SchoolYearRequest;
use App\Http\Controllers\Controller;
use App\Models\school_year;

class SchoolYearController extends Controller
{
    public function index()
    {
        $school_year = school_year::all();
        return response()->json($school_year, 200);
    }
   
    public function store(SchoolYearRequest $request)
    {
        $school_year = school_year::create($request->all());
        return response()->json($school_year, 201);
    }
}
