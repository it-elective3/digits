<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SectionsRequest;
use App\Http\Controllers\Controller;
use App\Models\sections;

class SectionsController extends Controller
{
    public function index()
    {
        $sections = sections::all();

        return response()->json($sections, 200);
    }

    public function store(SectionsRequest $request)
    {
        $sections = gender::create($request->all());
        return response()->json($sections, 201);
    }
}
