<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\SanctionsRequest;
use App\Http\Controllers\Controller;
use App\Models\sanction;

class SanctionsController extends Controller
{
    public function index()
    {
        $sanction = DB::table('sanctions')
            ->join('students', 'students.student_id', '=', 'sanctions.student_id')
            ->join('sections', 'sections.section_id', '=', 'students.section_id')
        ->select('sanctions.*', 'students.*', 'sections.*')
        ->orderBy('students.last_name', 'asc')->get();

        return response()->json($sanction, 200);
    }
   
    public function store(SanctionsRequest $request)
    {
        $sanction = sanction::create($request->all());
        return response()->json($sanction, 201);
    }

    public function update(SanctionsRequest $request, $id)
    {
        $sanction = sanction::where('sanction_id', $id)->update($request->all(), $id);
        return response()->json($sanction, 200);
    }
}
