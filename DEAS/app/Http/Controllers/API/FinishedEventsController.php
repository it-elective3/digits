<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\FinishedEventsRequest;
use App\Http\Controllers\Controller;
use App\Models\finished_events;

class FinishedEventsController extends Controller
{
    public function index()
    {
        $finished_events = DB::table('finished_events')
            ->join('events', 'events.event_id', '=', 'finished_events.event_id')
        ->select('finished_events.event_id', 'events.*')
        ->orderBy('finished_events.finished_event_id', 'asc')->get();

        return response()->json($finished_events, 200);
    }

    public function store(FinishedEventsRequest $request)
    {
        $finished_events = finished_events::create($request->all());
        return response()->json($finished_events, 201);
    }
}
