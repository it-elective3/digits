<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AttendanceRequest;
use App\Http\Controllers\Controller;
use App\Models\attendance;

class AttendanceController extends Controller
{
    public function index()
    {
        $attendance = DB::table('attendances')
            ->join('students', 'students.student_id', '=', 'attendances.student_id')
            ->join('sections', 'sections.section_id', '=', 'students.section_id')
            ->join('finished_events', 'finished_events.finished_event_id', '=', 'attendances.finished_event_id')
            ->join('events', 'events.event_id', '=', 'finished_events.finished_event_id')
        ->select('attendances.*', 'students.*', 'sections.*', 'events.event_name', 'finished_events.finished_event_id')->get();

        return response()->json($attendance, 200);
    }
   
    public function store(AttendanceRequest $request)
    {
        $attendance = attendance::create($request->all());
        return response()->json($attendance, 201);
    }

    public function update(AttendanceRequest $request, $id)
    {
        $attendance = attendance::where('attendance_id', $id)->update($request->all(), $id);
        return response()->json($attendance, 200);
    }
}
