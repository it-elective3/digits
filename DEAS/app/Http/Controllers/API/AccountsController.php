<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\AccountsRequest;
use App\Http\Controllers\Controller;
use App\Models\accounts;

class AccountsController extends Controller
{
    public function index()
    {
        $accounts = accounts::all();

        return response()->json($accounts, 200);
    }

    public function store(AccountsRequest $request)
    {
        $accounts = new accounts;
        $accounts -> position_id = request('position_id');
        $accounts -> student_id = request('student_id');
        $accounts -> password = bcrypt(request('password'));
        $accounts -> save();

        return response()->json($accounts, 201);
    }
}
