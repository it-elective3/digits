<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EventRequest;
use App\Http\Controllers\Controller;
use App\Models\event;

class EventController extends Controller
{
    public function index()
    {
        $event = DB::table('events')
        ->join('semesters', 'semesters.semester_id', '=', 'events.semester_id')
        ->join('school_years', 'school_years.school_year_id', '=', 'events.school_year_id')
        ->select('events.*', 'semesters.*', 'school_years.*')->get();

        return response()->json($event, 200);
    }
   
    public function store(EventRequest $request)
    {
        $event = event::create($request->all());
        return response()->json($event, 201);
    }

    public function update(EventRequest $request, $id)
    {
        $event = event::where('event_id', $id)->update($request->all(), $id);
        return response()->json($event, 200);
    }
}
