<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\AttendanceStatusRequest;
use App\Http\Controllers\Controller;
use App\Models\attendance_status;

class AttendanceStatusController extends Controller
{
    public function index()
    {
        $attendance_status = attendance_status::all();
        return response()->json($attendance_status, 200);
    }
   
    public function store(AttendanceStatusRequest $request)
    {
        $attendance_status = attendance_status::create($request->all());
        return response()->json($attendance_status, 201);
    }

    public function update(AttendanceStatusRequest $request, $id)
    {
        $attendance_status = attendance_status::where('attendance_status_id', $id)->update($request->all(), $id);
        return response()->json($attendance_status, 200);
    }
}
