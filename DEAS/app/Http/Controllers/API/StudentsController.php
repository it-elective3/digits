<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StudentsRequest;
use App\Http\Controllers\Controller;
use App\Models\students;

class StudentsController extends Controller
{
    public function index()
    {
        $students = DB::table('students')
            ->join('year_levels', 'year_levels.year_id', '=', 'students.year_id')
            ->join('sections', 'sections.section_id', '=', 'students.section_id')
            ->join('genders', 'genders.gender_id', '=', 'students.gender_id')
        ->select('students.*', 'year_levels.year_description', 'sections.section_description', 'genders.gender_description')
        ->orderBy('students.last_name', 'asc')->get();
        
        return response()->json($students, 200);
    }

    public function store(StudentsRequest $request)
    {
        $students = students::create($request->all());
        return response()->json($students, 201);
    }
}
