<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class finished_events extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function eventRelation(){
        return $this->hasOne(event::class, 'event_id', 'event_id');
    }

}
