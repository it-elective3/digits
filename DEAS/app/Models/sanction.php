<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sanction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function studentRelation(){
        return $this->hasOne(students::class, 'student_id', 'student_id');
    }
}
