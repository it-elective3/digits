<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class school_year extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function eventRelation(){
        return $this->hasOne(event::class, 'school_year_id', 'school_year_id');
    }
}
