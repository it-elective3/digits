<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class attendance extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function studentsRelation(){
        return $this->hasMany(students::class, 'student_id', 'student_id');
    }

    public function finishedEventRelation(){
        return $this->hasOne(finished_events::class, 'event_id', 'finished_event_id');
    }

    public function timeInRelation(){
        return $this->hasOne(attendance_status::class, 'time_in_status', 'status_id');
    }

    public function timeOutRelation(){
        return $this->hasOne(attendance_status::class, 'time_out_status', 'status_id');
    }
}
