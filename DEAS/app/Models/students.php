<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function yearRelation(){
        return $this->hasOne(year_level::class, 'year_id', 'year_id');
    }

    public function genderRelation(){
        return $this->hasMany(gender::class, 'gender_id', 'gender_id');
    }

    public function sectionsRelation(){
        return $this->hasMany(sections::class, 'section_id', 'section_id');
    }
}
