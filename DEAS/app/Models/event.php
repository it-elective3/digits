<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function semesterRelation(){
        return $this->hasOne(semester::class, 'semester_id', 'semester_id');
    }

    public function schoolyearRelation(){
        return $this->hasMany(school_year::class, 'school_year_id', 'school_year_id');
    }

    public function studentsRelation(){
        return $this->hasMany(students::class, 'student_monitor_id', 'student_id');
    }
}
