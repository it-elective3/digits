<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class semester extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function eventRelation(){
        return $this->hasOne(event::class, 'semester_id', 'semester_id');
    }
}
