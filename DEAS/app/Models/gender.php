<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gender extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function studentsRelation(){
        return $this->hasOne(students::class, 'gender_id', 'gender_id');
    }
}
