<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class accounts extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function positionRelation(){
        return $this->hasOne(position::class, 'position_id', 'position_id');
    }

    public function studentsRelation(){
        return $this->hasMany(students::class, 'student_id', 'student_id');
    }
}
