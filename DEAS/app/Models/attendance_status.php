<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class attendance_status extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function timeInRelation(){
        return $this->hasOne(attendance::class, 'status_id', 'time_in_status');
    }

    public function timeOutRelation(){
        return $this->hasOne(attendance::class, 'status_id', 'time_out_status');
    }
}
