<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class position extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function accountsRelation(){
        return $this->belongsTo(accounts::class, 'position_id', 'position_id');
    }
}
