<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\GenderController;
use App\Http\Controllers\API\SectionsController;
use App\Http\Controllers\API\StudentsController;
use App\Http\Controllers\API\YearLevelController;

use App\Http\Controllers\API\PositionController;
use App\Http\Controllers\API\AccountsController;

use App\Http\Controllers\API\SemesterController;
use App\Http\Controllers\API\EventController;
use App\Http\Controllers\API\SchoolYearController;

use App\Http\Controllers\API\FinishedEventsController;
use App\Http\Controllers\API\AttendanceController;
use App\Http\Controllers\API\SanctionsController;
use App\Http\Controllers\API\AttendanceStatusController;


Route::post('register', [AccountsController::class, 'register']);
Route::post('login', [AccountsController::class, 'login']);
Route::get('profile', [AccountsController::class, 'getAuthenticatedUser']);

Route::middleware('auth:api')->get('/accounts', function(Request $request){
    return $request->accounts();
});


Route::apiResource('gender', GenderController::class);
Route::apiResource('sections', SectionsController::class);
Route::apiResource('students', StudentsController::class);
Route::apiResource('year_level', YearLevelController::class);

Route::apiResource('position', PositionController::class);
Route::apiResource('accounts', AccountsController::class);

Route::apiResource('semester', SemesterController::class);
Route::apiResource('event', EventController::class);
Route::apiResource('school_year', SchoolYearController::class);

Route::apiResource('finished_event', FinishedEventsController::class);
Route::apiResource('attendance', AttendanceController::class);
Route::apiResource('sanctions', SanctionsController::class);
Route::apiResource('attendance_status', AttendanceStatusController::class);


