<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StudentsSeeder extends Seeder
{
    public function run()
    {
        $date = Carbon::now();
        $createdDate = clone($date);

        DB::table('students')->insert([
            ['student_id' => "1800656",
            'year_id' => "3",
            'gender_id' => "1",
            'section_id' => "15",
            'first_name' => "Harold Dave",
            'middle_name' => "Jayoma",
            'last_name' => "Limpiado",
            'age' => "21",
            'email' => "hlimpiado14@gmail.com",
            'contact_no' => "069654128386",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802946",
            'year_id' => "3",
            'gender_id' => "2",
            'section_id' => "18",
            'first_name' => "Anna Patriz",
            'middle_name' => "Negros",
            'last_name' => "Bareja",
            'age' => "21",
            'email' => "negrospat@gmail.com",
            'contact_no' => "09658742558",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802948",
            'year_id' => "3",
            'gender_id' => "1",
            'section_id' => "18",
            'first_name' => "Edralyn Mae",
            'middle_name' => "Abolencia",
            'last_name' => "Capales",
            'age' => "23",
            'email' => "capelesedra@gmail.com",
            'contact_no' => "0998609297",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802955",
            'year_id' => "3",
            'gender_id' => "1",
            'section_id' => "18",
            'first_name' => "Angelito",
            'middle_name' => "Abaño",
            'last_name' => "Domingo",
            'age' => "26",
            'email' => "ezkyuhiro24@gmail.com",
            'contact_no' => "09658742558",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],
        ]);                              
    }
}
