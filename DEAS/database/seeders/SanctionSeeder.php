<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SanctionSeeder extends Seeder
{
    public function run()
    {
        $date = Carbon::now();
        $createdDate = clone($date);

        DB::table('sanctions')->insert([
            ['student_id' => "1800656",
            'sanction_payables' => "0",
            'sanction_status' => "Pending",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802946",
            'sanction_payables' => "0",
            'sanction_status' => "Pending",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802948",
            'sanction_payables' => "0",
            'sanction_status' => "Pending",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['student_id' => "1802955",
            'sanction_payables' => "0",
            'sanction_status' => "Pending",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],
        ]); 
    }
}
