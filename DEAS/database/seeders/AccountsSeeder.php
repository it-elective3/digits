<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AccountsSeeder extends Seeder
{
    public function run()
    {
        $date = Carbon::now();
        $createdDate = clone($date);

        DB::table('accounts')->insert([
            ['position_id' => "1",
            'student_id' => "1800656",
            'password' => Hash::make("1800656"),
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['position_id' => "2",
            'student_id' => "1802946",
            'password' => Hash::make("1802946"),
            'created_at' => $createdDate,
            'updated_at' => $createdDate]
        ]);
    }
}
