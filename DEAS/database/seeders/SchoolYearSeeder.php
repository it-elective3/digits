<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SchoolYearSeeder extends Seeder
{
    public function run()
    {
        $schoolYears = [
            "2020-2021"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($schoolYears as $schoolYear) {
            DB::table('school_years')->insert(
                ['school_year_description' => $schoolYear,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}