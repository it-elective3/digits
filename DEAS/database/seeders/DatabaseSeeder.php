<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AttendanceStatusSeeder::class,
            GenderSeeder::class,
            PositionSeeder::class,
            SchoolYearSeeder::class,
            SectionsSeeder::class,
            SemesterSeeder::class,
            YearLevelSeeder::class,
            StudentsSeeder::class,
            AccountsSeeder::class,
        ]);

    }
}
