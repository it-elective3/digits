<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class YearLevelSeeder extends Seeder
{
    public function run()
    {
        $yearlevels = [
            "1st Year",
            "2nd Year",
            "3rd Year",
            "4th Year"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($yearlevels as $yearlevel) {
            DB::table('year_levels')->insert(
                ['year_description' => $yearlevel,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
