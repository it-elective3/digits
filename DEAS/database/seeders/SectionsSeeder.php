<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SectionsSeeder extends Seeder
{
    public function run()
    {
        $sections = [
            "AI11",
            "AI12",
            "AI13",
            "AI14",
            "AI15",
            "AI16",
            "AI17",

            "AI21",
            "AI22",
            "AI23",
            "AI24",
            "AI25",
            "AI26",
            "AI27",

            "AI31",
            "AI32",
            "AI33",
            "AI34",

            "F1",
            "F2"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($sections as $section) {
            DB::table('sections')->insert(
                ['section_description' => $section,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}