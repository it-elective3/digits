<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class GenderSeeder extends Seeder
{
    public function run()
    {
        $genders = [
            "Male",
            "Female"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($genders as $gender) {
            DB::table('genders')->insert(
                ['gender_description' => $gender,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
