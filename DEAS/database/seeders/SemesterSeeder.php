<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SemesterSeeder extends Seeder
{
    public function run()
    {
        $semesters = [
            "1st Semester",
            "2nd Semester"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($semesters as $semester) {
            DB::table('semesters')->insert(
                ['semester_description' => $semester,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}