<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AttendanceStatusSeeder extends Seeder
{
    public function run()
    {
        $status = [
            "Read",
            "Unread"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($status as $stat) {
            DB::table('attendance_statuses')->insert(
                ['status_description' => $stat,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
