<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PositionSeeder extends Seeder
{
    public function run()
    {
        $positions = [
            "Admin",
            "Treasurer"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($positions as $position) {
            DB::table('positions')->insert(
                ['description' => $position,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}