<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('student_id')->on('students');

            $table->integer('finished_event_id')->unsigned();
            $table->foreign('finished_event_id')->references('finished_event_id')->on('finished_events');

            $table->integer('time_in_status')->unsigned();
            $table->foreign('time_in_status')->references('status_id')->on('attendance_statuses');

            $table->integer('time_out_status')->unsigned();
            $table->foreign('time_out_status')->references('status_id')->on('attendance_statuses');

            $table->integer('attendance_payables');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
