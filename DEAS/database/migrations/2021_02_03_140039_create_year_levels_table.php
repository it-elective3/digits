<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYearLevelsTable extends Migration
{
    public function up()
    {
        Schema::create('year_levels', function (Blueprint $table) {
            $table->increments('year_id');
            $table->string('year_description');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('year_levels');
    }
}
