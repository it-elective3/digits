<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanctionsTable extends Migration
{
    public function up()
    {
        Schema::create('sanctions', function (Blueprint $table) {
            $table->increments('sanction_id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('student_id')->on('students');

            $table->integer('sanction_payables');
            $table->string('sanction_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sanctions');
    }
}
