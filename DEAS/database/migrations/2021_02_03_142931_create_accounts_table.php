<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('account_id');

            $table->integer('position_id')->unsigned();
            $table->foreign('position_id')->references('position_id')->on('positions');

            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('student_id')->on('students');

            $table->string('password');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
