<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('student_id');

            $table->integer('year_id')->unsigned();
            $table->foreign('year_id')->references('year_id')->on('year_levels');

            $table->integer('gender_id')->unsigned();
            $table->foreign('gender_id')->references('gender_id')->on('genders');

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('section_id')->on('sections');

            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->integer('age');
            $table->string('email');
            $table->string('contact_no');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('students');
    }
}
