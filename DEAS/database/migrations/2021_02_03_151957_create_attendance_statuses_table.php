<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceStatusesTable extends Migration
{
    public function up()
    {
        Schema::create('attendance_statuses', function (Blueprint $table) {
            $table->increments('status_id');
            $table->string('status_description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('attendance_statuses');
    }
}
