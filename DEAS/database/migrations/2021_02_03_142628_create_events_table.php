<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('event_id');

            $table->integer('semester_id')->unsigned();
            $table->foreign('semester_id')->references('semester_id')->on('semesters');

            $table->integer('school_year_id')->unsigned();
            $table->foreign('school_year_id')->references('school_year_id')->on('school_years');

            $table->integer('student_monitor_id')->unsigned();
            $table->foreign('student_monitor_id')->references('student_id')->on('students');

            $table->string('event_name');
            $table->string('event_date');
            $table->string('event_time_in');
            $table->integer('event_time_in_duration');
            $table->string('event_time_out');
            $table->integer('event_time_out_duration');
            $table->string('event_location');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('events');
    }
}
