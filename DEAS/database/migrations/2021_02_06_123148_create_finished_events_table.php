<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinishedEventsTable extends Migration
{
    public function up()
    {
        Schema::create('finished_events', function (Blueprint $table) {
            $table->increments('finished_event_id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('event_id')->on('events');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('finished_events');
    }
}
