
 google.charts.load('current', {'packages':['corechart', 'bar']});
google.charts.setOnLoadCallback(drawPieChart);
google.charts.setOnLoadCallback(drawBarChart);

  function drawPieChart() {
    var data = google.visualization.arrayToDataTable([
      ['Genre', 'Copies Borrowed',],
      ['Romance', 11],
      ['Mystery', 11],
      ['Sci-Fi', 11]
    ]);

    var options = {
      title: 'Popular Book Genres',
      pieHole: 0.4,
      colors:[ '#1B478A', '#EBF071', '#EF5D58']
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }


  function drawBarChart() {
    var data = google.visualization.arrayToDataTable([
      ['Month', 'Borrowed', 'Returned',],
      ['Jan', 20, 25],
      ['Feb', 15, 30 ],
      ['Mar', 15, 25 ],
      ['Apr', 15, 10],
      ['May', 30, 20],
      ['Jun', 25, 10]
    ]);

    var options = {
        title: 'Borrowed and Returned Books',
        colors:[ '#EF5D58', '#EBF071'],
    };

    var chart = new google.charts.Bar(document.getElementById('barchart'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }
