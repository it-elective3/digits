export default{

    componentKey: 0,

    activeButton: '',
    activeNavbar: '',
    adminLoginState: '',
    treasurerLoginState: '',
    adminID: '1800656',
    treasurerID: '1802946',

    finished_event_id: '',
    finished_section_name: '',
    finished_stud_id: '',
    finished_stud_ids: '',

    students:[],
    year_levels:[],
    genders:[],
    sections:[],

    positions:[],
    accounts:[],

    semesters:[],
    school_years:[],
    events:[],

    sanctions:[],
    attendance_statuses:[],
    attendances:[],
    finished_events:[]

}