import axios from "@/assets/js/axiosConfig";

//Events
export const fetchEvents = ({commit}) => {
    axios.get('/event')
        .then(response =>{
            commit('setEvents', response.data);
        })
}

export const createEvents = ({ commit }, data) => {
    axios.post('/event', data)
        .then(response =>{
       commit('createEvent', response.data);
    })
}

export const updateEvents = ({ commit }, event) => {
    axios.put(`/event/${event.event_id}`, event)
        .then(response =>{
       commit('updateEvent',  response.data);
       return response;
        })
}

//Students
export const fetchStudents = ({commit}) => {
  axios.get('/students')
      .then(response =>{
          commit('setStudents', response.data);
      })
}

//Gender
export const fetchGender = ({commit}) => {
  axios.get('/gender')
      .then(response =>{
          commit('setGender', eval(response.data));
      })
}

//Sections
export const fetchSections = ({commit}) => {
    axios.get('/sections')
        .then(response =>{
            commit('setSections', eval(response.data));
        })
}
  
//YearLevel
export const fetchYearLevel = ({commit}) => {
    axios.get('/year_level')
        .then(response =>{
            commit('setYearLevel', eval(response.data));
        })
}

export const fetchSchoolYear = ({commit}) => {
    axios.get('/school_year')
        .then(response =>{
            commit('setSchoolYear', eval(response.data));
        })
}

export const fetchSemester = ({commit}) => {
    axios.get('/semester')
        .then(response =>{
            commit('setSemester', eval(response.data));
        })
}



export const fetchAttendance = ({commit}) => {
    axios.get('/attendance')
        .then(response =>{
            commit('setAttendances', response.data);
        })
}

export const fetchEvent = ({commit}) => {
    axios.get('/event')
        .then(response =>{
            commit('setEvent', eval(response.data));
        })
}

export const fetchAttendanceStatus = ({commit}) => {
    axios.get('/attendance_status')
        .then(response =>{
            commit('setAttendanceStatus', response.data);
        })
}

export const fetchFinishedEvent = ({commit}) => {
    axios.get('/finished_event')
        .then(response =>{
            commit('setFinishedEvent', eval(response.data));
        })
}

export const fetchSanctions = ({commit}) => {
    axios.get('/sanctions')
        .then(response =>{
            commit('setSanctions', eval(response.data));
        })
}

export const fetchAccounts = ({commit}) => {
    axios.get('/accounts')
        .then(response =>{
            commit('setAccounts', eval(response.data));
        })
}

export const updateSanctions = ({ commit }, sanction) => {
    axios.put(`/sanctions/${sanction.sanction_id}`, sanction)
        .then(response =>{
       commit('updateSanction',  response.data);
       return response;
        })
}