export const getButton = (state => {
  return state.activeButton;
})

export const  getTitle = (state => {
  return state.activeNavbar;
})

export const  getAdminLogin = ( state => {
  return state.adminLoginState;
})

export const  getTreasurerLogin = ( state => {
  return state.treasurerLoginState;
})


export const  getGenders = ( state => {
  return state.genders;
})

export const  getSections = ( state => {
  return state.sections;
})

export const  getYearLevel = ( state => {
  return state.year_levels;
})

export const  getEvents = ( state => {
  return state.events;
})

export const  getStudents = ( state => {
  return state.students;
})

export const  getAttendanceStatus = ( state => {
  return state.attendance_statuses;
})

export const  getFinishedEvent = ( state => {
  return state.finished_events;
})

export const  getSanctions= ( state => {
  return state.sanctions;
})

export const  getAttendances= ( state => {
  return state.attendances;
})

export const  getSchoolYear= ( state => {
  return state.school_years;
})

export const  getSemesters= ( state => {
  return state.semesters;
})

export const  getComponentKey = ( state => {
  return state.componentKey;
})

export const  getAccounts = ( state => {
  return state.accounts;
})


