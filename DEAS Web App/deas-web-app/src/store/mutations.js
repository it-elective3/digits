//Navigation Mutations----------------------------------------
export const setButton = (state, name) => {
  state.activeButton = name;
}

export const setNavbar = (state, name) => {
  state.activeNavbar = name;
}

export const setAdminLogin = (state, bool) => {
  state.adminLoginState = bool;
}

export const setTreasurerLogin = (state, bool) => {
  state.treasurerLoginState = bool;
}



//Events--------------------------------------------------
export const setEvents = (state, events) =>{
    state.events = events
}

export const createEvent = (state, data) =>{
    state.events.unshift({ ...data })
}

export const updateEvent = (state, updateEvents) =>{
    let i = state.events.findIndex((event) => event.event_id === updateEvents.event_id);
    state.events.splice(i, 1, updateEvents);
}

// Students-------------------------------------------
export const setStudents = (state, student) =>{
  state.students = student
}

// Students-------------------------------------------
export const setGender = (state, gender) =>{
  state.genders = gender
}

export const setSections = (state, sections) =>{
  state.sections = sections
}

export const setYearLevel = (state, year_level) =>{
  state.year_levels = year_level
}

export const setSchoolYear = (state, year) =>{
  state.school_years = year
}

export const setSemester = (state, semester) =>{
  state.semesters = semester
}


export const setAttendances = (state, attendances) =>{
  state.attendances = attendances
}

export const setEvent = (state, event) =>{
  state.events = event
}

export const setAttendanceStatus = (state, attendance_status) =>{
  state.attendance_statuses = attendance_status
}

export const setFinishedEvent = (state, finished_event) =>{
  state.finished_events = finished_event
}

export const setFinishedEventId = (state, finished_event_id) =>{
  state.finished_event_id = finished_event_id
}

export const setSectionId = (state, finished_section_id) =>{
  state.finished_section_name = finished_section_id
}

export const setStudId = (state, finished_stud_id) =>{
  state.finished_stud_id = finished_stud_id
}

export const setStudIds = (state, finished_stud_ids) =>{
  state.finished_stud_ids = finished_stud_ids
}

export const setSanctions = (state, sanction) =>{
  state.sanctions = sanction
}

export const setComponentKey = (state, key) =>{
  state.componentKey = key
}

export const setAccounts = (state, account) =>{
  state.accounts = account
}

export const updateSanction = (state, updateSanctions) =>{
  let i = state.sanctions.findIndex((sanction) => sanction.sanction_id === updateSanctions.sanction_id);
  state.sanctions.splice(i, 1, updateSanctions);
}
