import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login/login.vue";
import Dashboard from "../views/Dashboard/dashboard.vue";
import Attendance_1 from "../views/Attendance/attendance-1.vue";
import Attendance_2 from "../views/Attendance/attendance-2.vue";
import Sanctions_1 from "../views/Sanctions/sanctions-1.vue";
import Sanctions_2 from "../views/Sanctions/sanctions-2.vue";
import Sanctions_3 from "../views/Sanctions/sanctions-3.vue";
import Students from "../views/Students/student.vue";
import store from "@/store/index.js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/Dashboard",
    name: "Dashboard",
    component: Dashboard,
    alias: "/D",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/LogIn",
    name: "Login",
    component: Login,
    alias: "/L"
  },
  {
    path: "/Attendance-1",
    name: "Attendance-1",
    component: Attendance_1,
    alias: "/A1",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/Attendance-2",
    name: "Attendance-2",
    component: Attendance_2,
    alias: "/A2",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/Sanctions-1",
    name: "Sanctions-1",
    component: Sanctions_1,
    alias: "/S1",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/Sanctions-2",
    name: "Sanctions-2",
    component: Sanctions_2,
    alias: "/S2",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/Sanctions-3",
    name: "Sanctions-3",
    component: Sanctions_3,
    alias: "/S3",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },
  {
    path: "/Students", 
    name: "Students",
    component: Students,
    alias: "/ST",
    beforeEnter (to, from, next) {
      if(store.getters.getLogin == false) { 
        next("/L");
      } else {
        next();
      }
    }
  },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
